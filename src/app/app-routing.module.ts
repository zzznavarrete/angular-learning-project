import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';


import { HomeComponent } from './home/components/home/home.component';
import { ContactComponent } from './contact/contact.component';
import { ProductsComponent } from './products/components/products/products.component';
import { DemoComponent } from './demo/demo.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ProductDetailComponent } from './products/components/product-detail/product-detail.component';
import { LayoutComponent } from './layout/layout.component';

import { AdminGuard } from './admin.guard';
const routes: Routes = [
{
  path: '',
  component: LayoutComponent,
  children: [
{
  path: '',
  redirectTo: '/home',
  pathMatch: 'full'
},
{
  path: 'home',
  loadChildren: () => import('./home/home.module').then(m => m.HomeModule)
  //component: HomeComponent
},
{
  path: 'products',
  loadChildren: () => import('./products/products.module').then(m => m.ProductsModule)
},
{
  path: 'products/:id',
  component: ProductDetailComponent
},

{
  path: 'contact',
  canActivate: [AdminGuard],
  component: ContactComponent
},
]
},
{
  path: 'demo',
  component: DemoComponent
},
{
  path: 'admin',
  loadChildren: () => import('./admin/admin.module').then(m => m.AdminModule)
},
  //Este path siempre tiene que ir al último
{
  //Incluyendo un 404 not found
  path: '**', //doble asterisco significa que no hubo match
  component: PageNotFoundComponent
}

  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules
  })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
