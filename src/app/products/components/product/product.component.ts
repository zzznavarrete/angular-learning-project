import { Component,
    Input,
    Output, 
    EventEmitter, 
    OnChanges,
    SimpleChanges, 
    OnInit, 
    DoCheck, 
    OnDestroy} from '@angular/core';

import { Product } from './../../../product.model';

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.scss']
})
export class ProductComponent implements  OnInit, DoCheck, OnDestroy {//,OnChanges{
   @Input() product: Product;
   @Output() productClickled: EventEmitter<any> = new EventEmitter();

    today = new Date();

   //Es lo primero que se ejcuta
    constructor(){
        console.log('CONSTRUCTOR');
    }

    //Es lo segundo que se ejecuta
  //  ngOnChanges(changes: SimpleChanges){
    //    console.log('ON CHANGE');
        //Imprimo cuales fueron los cambios
      //  console.log(changes);
   // }

    ngOnInit(){
        console.log('3.ngOnInit');
    }

    ngDoCheck(){
        console.log('4. ngDoCheck');
    }

    ngOnDestroy(){
        console.log('5. ngOnDestroy');
    }

   addToCart(){
    console.log('añadir al carrito');
    this.productClickled.emit(this.product.id);
   }



}

