import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    console.log('onInit');
  }

  title = 'beta-store';
  test = 'variable test';
  items = ['item1', 'item2', 'item3'];



  power = 10;

  addItem(){
    this.items.push('nuevo item');
  }

  deleteItem(index: number){
    this.items.splice(index,1);
  }

}
